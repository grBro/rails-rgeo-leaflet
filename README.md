# Drawing on maps with Rails, Leaflet and RGeo #

# Little [Demo Video](http://c2n.me/3lzI01x)

### Install: ###

Install PostgreSQL 9.4 + PostGIS 2.1

    git clone git@bitbucket.org:grBro/rails-rgeo-leaflet.git
    
    bundle install

    bower install

    bundle exec rake db:create

    bundle exec rake db:migrate