class Field < ActiveRecord::Base
  validates :name, presence: true

  def self.proper_geometry(shape)
    return true if shape.empty? || JSON.parse(shape)['coordinates'].empty?
    wkt_shape = self.get_wkt(shape)
    sql = "SELECT ST_IsValid(ST_GeomFromText('#{wkt_shape}'))"
    sql_reason = "SELECT ST_IsValidReason(ST_GeomFromText('#{wkt_shape}'))"
    result = ActiveRecord::Base.connection.execute(sql)
    reason = ActiveRecord::Base.connection.execute(sql_reason)
    if result.getvalue(0,0) == 'f'
      begin
      raise reason.getvalue(0,0)
      rescue Exception => e
        exception = e.message
      end
      exception
    else
      true
    end
  end

  def self.get_wkt(value)
    JSON.parse(value)['coordinates'].to_wkt_multi_polygon
  end

  def self.to_geographic(shape)
    self.load_factory.multi_polygon(RGeo::GeoJSON.decode(shape, :json_parser => :json))
  end

  def shape_to_json_geo
    RGeo::GeoJSON.encode(self.shape).to_json.html_safe
  end

  def self.load_multipolygon_arr
    arr = []
    self.all.each do |field|
      shape = RGeo::GeoJSON.encode(field.shape)
      arr << shape
    end
    arr.to_json.html_safe
  end

  def self.load_factory
    RGeo::Geographic.spherical_factory(:srid => 4326)
  end
end
