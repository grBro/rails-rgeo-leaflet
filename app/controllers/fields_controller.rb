class FieldsController < ApplicationController
  before_action :load_field, only: [:show, :edit, :update, :destroy]
  before_action :validate_and_load_shape, only: [:create, :update]

  def index
    @fields = Field.all
    @geo_data = Field.load_multipolygon_arr
  end

  def new
    @field = Field.new
  end

  def create
    @field = Field.new(name: field_params[:name], shape: @shape)

    if @field.save
      redirect_to @field, notice: 'Field was successfully created.'
    else
      render :new
    end
  end

  def show
    @geo_data = @field.shape_to_json_geo
  end

  def edit
    @field.shape = @field.shape_to_json_geo
  end

  def update
    @shape ||= @field.shape
    if @field.update(name: field_params[:name], shape: @shape)
      @field.errors.add(:invalid_geometry, @exception) unless @exception.nil?
      respond_to do |format|
        format.html { redirect_to field_path, notice: 'Field was successfully updated.' }
        format.js
      end
    else
      render :new
    end
  end

  def destroy
    @field.destroy
    redirect_to root_path, notice: 'Field was successfully destroyed.'
  end

  private

    def load_field
      @field = Field.find(params[:id])
    end

    def validate_and_load_shape
      result = Field.proper_geometry(field_params[:shape])
      if result == true
        @shape = Field.to_geographic(field_params[:shape])
      else
        @exception = result
      end
    end

    def field_params
      params.require(:field).permit(:name, :shape)
    end
end
